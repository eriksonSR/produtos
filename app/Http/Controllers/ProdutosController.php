<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produto;
use Session;

class ProdutosController extends Controller
{
	public function index()
	{
		$produtos = Produto::all();
		return view('produto.index', array('produtos' => $produtos));
	}

	public function create()
	{
		return view('produto.create');
	}

	public function store(Request $request)
	{
		$produto = new Produto();

		if($request->hasFile('fotoproduto')){
			$imagem = $request->file('fotoproduto');
			$nomearquivo = md5($id) . '.' . $imagem->getClientOriginalExtension();
			$request->file('fotoproduto')->move(public_path('./img/produtos/'), $nomearquivo);
		}

		$produto->titulo = $request->input('titulo');
		$produto->preco = $request->input('preco');
		$produto->categoria = $request->input('categoria');
		
		if($produto->save()){
			Session::flash('mensagem', 'Produto cadastrado com sucesso.');
			return redirect('produtos');
		}
	}

	public function edit($id)
	{
		$produto = Produto::find($id);
		return view('produto.edit', array('produto' => $produto));
	}

	public function update($id, Request $request)
	{
		$produto = Produto::find($id);

		if($request->hasFile('fotoproduto')){
			$imagem = $request->file('fotoproduto');
			$nomearquivo = md5($id) . '.' . $imagem->getClientOriginalExtension();
			$request->file('fotoproduto')->move(public_path('./img/produtos/'), $nomearquivo);
		}

		$produto->titulo = $request->input('titulo');
		$produto->preco = $request->input('preco');
		$produto->categoria = $request->input('categoria');
		
		$produto->save();
		Session::flash('mensagem', 'Produto alterado com sucesso.');
		return redirect()->back();
	}

	public function destroy($id)
	{
		$produto = Produto::find($id);
		$produto->delete();
		Session::flash('mensagem', 'Produto excluído com sucesso.');
		return redirect()->back();
	}
}
