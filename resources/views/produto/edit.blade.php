@extends('layouts.app')
@section('title', 'Editando o produto: ' . $produto->titulo)
@section('content')
	<h1>Editar o produto: {{$produto->titulo}}</h1>
	@if(Session::has('mensagem'))
		<div class="alert alert-success">
			{{Session::get('mensagem')}}
		</div>
	@endif
	@if(count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{{Form::open(['route' => ['produtos.update', $produto->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT'])}}
	{{Form::label('titulo', 'Titulo')}}
	{{Form::text('titulo', $produto->titulo, ['class'=>'form-control', 'required', 'placeholder' => 'Titulo'])}}
	{{Form::label('preco', 'Preço')}}
	{{Form::text('preco', $produto->preco, ['class' => 'form-control', 'required', 'placeholder' => 'Preço'])}}
	{{Form::label('categoria', 'Categoria')}}
	{{Form::text('categoria', $produto->categoria, ['class'=>'form-control', 'required', 'placeholder' => 'Categoria'])}}
	{{Form::label('fotoproduto', 'Foto')}}
	{{Form::file('fotoproduto', ['class' => 'form-control', 'id' => 'fotoproduto'])}}
	<br/>
	{{Form::submit('Alterar', ['class' => 'btn btn-default'])}}
	{{Form::close()}}
@endsection