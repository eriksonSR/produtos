@extends('layouts.app')
@section('title', 'Cadastrar um produto')
@section('content')
	<h1>Cadastrar produto</h1>
	@if(count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{{Form::open(['action' => 'ProdutosController@store'])}}
	{{Form::label('titulo', 'Titulo')}}
	{{Form::text('titulo', '', ['class'=>'form-control', 'required', 'placeholder' => 'Titulo'])}}
	{{Form::label('preco', 'Preço')}}
	{{Form::text('preco', '', ['class' => 'form-control', 'required', 'placeholder' => 'Preço'])}}
	{{Form::label('categoria', 'Categoria')}}
	{{Form::text('categoria', '', ['class'=>'form-control', 'required', 'placeholder' => 'Categoria'])}}
	<br/>
	{{Form::submit('Cadastrar', ['class' => 'btn btn-default'])}}
	{{Form::close()}}
@endsection