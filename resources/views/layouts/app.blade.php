<!DOCTYPE html>
<html>
<head>
	<title>Catálogo de produtos - @yield('title')</title>
	{{Html::style('css/bootstrap.min.css')}}
	{{Html::style('css/bootstrap-theme.min.css')}}
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	{{Html::script('js/jquery.min.js')}}
	{{Html::script('js/bootstrap.min.js')}}
	{{Html::script('js/produtos.js')}}
</body>
</html>